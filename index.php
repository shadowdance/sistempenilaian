<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Halaman Awal</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css">

    <!-- Custom styles for this template -->
    <link href="css/freelancer.min.css" rel="stylesheet">

    <link rel="icon" type="image/png" href="img/logo.jpg">

  </head>

  <body id="page-top">

    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav" style="background:#F9BF3B">
      <div class="container">
        <a class="navbar-brand js-scroll-trigger" href="#page-top">SD Permata Insani</a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          Menu
          <i class="fa fa-bars"></i>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <!-- TOMBOL -->
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#portfolio">Struktur Organisasi</a>
            </li>
            <!-- END TOMBOL -->
            <!-- TOMBOL -->
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#about">Visi & Misi</a>
            </li>
            <!-- END TOMBOL -->
            <!-- TOMBOL -->
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#contact">Kontak</a>
            </li>
            <!-- END TOMBOL -->
            <!-- TOMBOL -->
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="dashboard/loginadmin.php">Login</a>
            </li>
            <!-- END TOMBOL -->
          </ul>
        </div>
      </div>
    </nav>

    <!-- Header -->
    <header class="masthead">
      <div class="container">
        <img class="img-fluid" src="img/logo.jpg" alt="" width="20%" height="20%">
        <div class="intro-text">
          <span class="name">SD PERMATA INSANI</span>
          <span class="skills">Islamic School</span>
        </div>
      </div>
    </header>

    <!-- Portfolio Grid Section -->
    <section id="portfolio">
      <div class="container">
        <h2 class="text-center">Struktur Organisasi</h2>
        <hr class="star-primary">
          <center><img src="img/strukturorganisasi.jpg"></center>
      </div>
    </section>

    <!-- About Section -->
    <section class="success" id="about">
      <div class="container">
        <h2 class="text-center">Visi & Misi</h2>
        <hr class="star-light">
        <div class="row">
          <div class="col-lg-4 ml-auto">
            Visi:
            <p>Meraih prestas dibidang IMTAKi dan IPTEK melalui pendidikan yang menitikberatkan pendidikan umat dengan iman ilmu dan amal menuju sekolah bertaraf nasional yang berwawasan global.</p>
          </div>
          <div class="col-lg-4 mr-auto">
            Misi:
            <p>- Membantu perkembangan karakter Omat melalui sikap perilaku pengetahuan keterampilan dan daya cipta sebagai dasar pembentuk generasi yang cerdas.<br><br>
            - Memberikan bekal pengetahuan yang berlandaskan keimanan dan ketakwaan serta keintelektualan melalui pendidikan keagamaan.<br><br>
            - Menjadi sekolah yang unggul dalam Imtak dan IPTEK sebagai sekolah bertaraf nasional dengan wawasan global
            </p>
          </div>
        </div>
      </div>
    </section>

    <!-- Contact Section -->
    <section id="contact">
      <div class="container">
        <h2 class="text-center">Kontak Kami</h2>
        <hr class="star-primary">
        <div class="row">
          <div class="col-lg-8 mx-auto">
            <h4 class="text-center">Alamat : Sindangsari, Pasarkemis, Tangerang.<br><br>Telp. (021) 59350234</h4>
          </div>
        </div>
      </div>
    </section>

    <!-- Footer -->
    <footer class="text-center">
      <div class="footer-below" style="background:#F9BF3B">
        <div class="container">
          <div class="row">
            <div class="col-lg-12">
              Copyright &copy;2017
            </div>
          </div>
        </div>
      </div>
    </footer>

    <!-- Scroll to Top Button (Only visible on small and extra-small screen sizes) -->
    <div class="scroll-top d-lg-none">
      <a class="btn btn-primary js-scroll-trigger" href="#page-top">
        <i class="fa fa-chevron-up"></i>
      </a>
    </div>


    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/popper/popper.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Contact Form JavaScript -->
    <script src="js/jqBootstrapValidation.js"></script>
    <script src="js/contact_me.js"></script>

    <!-- Custom scripts for this template -->
    <script src="js/freelancer.min.js"></script>

  </body>

</html>
