<?php
    include('header.php');
?>

<?php
//cek jika url index.php tidak ada nilai
if(isset($_GET['hal'])){
    //berikan nilai index.php?hal
    switch($_GET['hal']){
        //saat index?hal=beranda, tampilkan isi konten dari beranda.php
        case 'beranda'  : include 'beranda.php'; break;

        //saat index?hal=gr, tampilkan isi konten dari data/profile.php
        case 'pr'      : include 'data/profile.php'; break;

        //saat index?hal=sw, tampilkan isi konten dari data/murid.php
        case 'mr'      : include 'data/murid.php'; break;

        //saat index?hal=pw, tampilkan isi konten dari data/gantipassword.php
        case 'pw'      : include 'data/gantipassword.php'; break;

        //saat index?hal=nl, tampilkan isi konten dari mod_nilai/entrynilai.php
        case 'dn'      : include 'mod_nilai/datanilai.php'; break;
        case 'un'      : include 'mod_nilai/ubahnilai.php'; break;
        case 'en'      : include 'mod_nilai/entrynilai.php'; break;

        case 'cr'      : include 'mod_nilai/raport.php'; break;


        //selain itu tampilkan konten dari 404.php
        default : include '404.php';
    }
    
}else{
    //halaman default dari index berisi konten beranda.php
    include 'beranda.php';
}
?>

<?php
    include('mod_nilai/updatenilai.php');
?>

<?php
    include('data/detilmurid.php');
    include('footer.php');
?>