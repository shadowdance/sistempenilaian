<?php	
	ob_start();
	include('../../connection/connection.php');

	require ("../../html2pdf/html2pdf.class.php");
	$content = ob_get_clean();
	$sql = $db->query("SELECT a.*, b.nama as nmsw, b.nis as nissw, c.nama_kelas, d.nama_mapel, e.tahun_ajaran, f.nama as nmguru
					   from nilai a, siswa b, kelas c, mata_pelajaran d, tahun_ajaran e, guru f
					   where a.nis=b.nis and a.nip=f.nip and a.kd_mapel=d.kd_mapel
					   and b.kd_kelas=c.kd_kelas and c.kd_ta=e.kd_ta
					   and b.nis='$_GET[nis]' and a.semester='$_GET[semester]'");
	$sql->execute();
	$row = $sql->fetch(PDO::FETCH_ASSOC);
	$content = "<img src='../../../img/kop.jpg' height='150' width='780'>
				<hr width='100%'>
				<p align='center'>
				<b style='font-size: 20'>RAPORT SISWA</b><br>
				</p>
				<p style='margin-left:50px;'>
				<table cellpadding='0' cellspacing='1' style='width: 210mm;' border=0>
					<tr>
						<td>Nama</td>
						<td> : </td>
						<td style='text-transform:capitalize;'><b> $row[nmsw] </b></td>
						<td>&nbsp;&nbsp;&nbsp;</td>
						<td>Kelas</td>
						<td style='text-transform:capitalize;'><b> : $row[nama_kelas] </b></td>
					</tr>
					<tr>
						<td>NIS</td>
						<td> : </td>
						<td style='text-transform:capitalize;'><b> $row[nissw] </b></td>
						<td>&nbsp;&nbsp;&nbsp;</td>
						<td>Semester</td>
						<td><b> : $row[semester] </b></td>
					</tr>
					<tr>
						<td>Nama Sekolah</td>
						<td> : </td>
						<td><b> SD Permata Insani Islamic School </b></td>
						<td>&nbsp;&nbsp;&nbsp;</td>
						<td>Tahun Ajaran</td>
						<td><b> : $row[tahun_ajaran] </b></td>
					</tr>
					<tr>
						<td>Alamat Sekolah</td>
						<td> : </td>
						<td><b> Perum Villa Permata Blok G1 Kel. Sindangsari </b></td>
					</tr>
				</table><br>
				<table cellpadding='0' cellspacing='1' style='width: 210mm;' border=0.5>
					<tr bgcolor='#CCCCCC'>
						<th style='width: 20px;' align='center'>NO</th>
						<th style='width: 150px;' align='center'>Mata Pelajaran</th>
						<th style='width: 150px;' align='center'>Nilai</th>
						<th style='width: 150px;' align='center'>Predikat</th>
						<th style='width: 150px;' align='center'>Keterangan</th>
					</tr>";
	$sql->execute();
	$i=1;
	while($row = $sql->fetch(PDO::FETCH_ASSOC))
		{
			if($row['nil_akhir']>='0' and $row['nil_akhir']<'70'){
				$predikat = 'D';
			}else if ($row['nil_akhir']>='70' and $row['nil_akhir']<'80') {
				$predikat = 'C';
			}else if ($row['nil_akhir']>='80' and $row['nil_akhir']<'90') {
				$predikat = 'B';
			}else{
				$predikat = 'A';
			}
			$content.="<tr bgcolor='#FFFFFF'>
						<td align='center'>".$i."</td>
						<td style='text-transform:capitalize; text-align: center;'>".$row['nama_mapel']."</td>
						<td align='center'>".$row['nil_akhir']."</td>
						<td style='text-transform:capitalize;text-align: center;'>".$predikat."</td>
						<td style='text-transform:capitalize;text-align: center;'>".$row['keterangan']."</td>
					   </tr>";	
			$i++;		
		}							
    $content.="</table></p>";
	ob_end_clean();
	// conversion HTML => PDF
	try
	{
		$html2pdf = new HTML2PDF('P', 'A4','fr', false, 'ISO-8859-15'); 
		$html2pdf->setDefaultFont('Arial');
		$html2pdf->writeHTML($content);
		$html2pdf->Output('raport.pdf');
	}
	catch(HTML2PDF_exception $e) { echo $e; }
?>