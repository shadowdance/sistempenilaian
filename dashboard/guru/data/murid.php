            <div class="panel panel-default">
              <?php
                    $hasil = $db->query("SELECT a.*, b.nama_kelas from guru a, kelas b where a.nip = b.nip and a.nip='$_SESSION[guru]'");

                    $r = $hasil->fetch(PDO::FETCH_ASSOC);
              ?>
              <div class="panel-body"><h4><i class="fa fa-users fa-fw"></i>Data Murid Kelas <b style="text-transform:uppercase;"><?php echo $r['nama_kelas'];?></b></h4></div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <?php
                                include('../connection/connection.php');
                                //query untuk menampilkan data murid yang wali kelasnya adalah guru yg login
                                $stmt = $db->query("SELECT a.nis, a.nama as nmsw, a.jenkel as jksw, a.alamat as alsw, b.*, c.*, d.nip FROM siswa a, kelas b, tahun_ajaran c, guru d where a.kd_kelas = b.kd_kelas and b.kd_ta = c.kd_ta and b.nip = d.nip and d.nip='$_SESSION[guru]'");
                                echo'<table style="table-layout:fixed;" class="table table-striped table-bordered table-hover" id="dataTables-example">';
                                    echo '<thead>';
                                        echo '<tr>';
                                            echo '<th>NIS</th>';
                                            echo '<th>Nama</th>';
                                            echo '<th>Jenis Kelamin</th>';
                                            echo '<th>Alamat</th>';
                                            echo '<th>Aksi</th>';
                                        echo '</tr>';
                                    echo '</thead>';
                                    echo '<tbody>';
                                    while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                                            echo "<tr'>";       
                                                echo "<form method='POST' action='mod_siswa/proses.php'>";
                                                echo "<td style=' width:150px;  text-align:left; padding: 10px;vertical-align: middle;' class='text-uppercase'>";echo $row['nis'];echo"</td>";
                                                echo "<td style='width:110px;  text-align:left; vertical-align: middle;' class='text-capitalize'>";echo $row['nmsw'];"</td>";
                                                echo "<td style='width:110px;  text-align:left; vertical-align: middle;' class='text-capitalize'>";echo $row['jksw'];"</td>";
                                                echo "<td style='width:110px;  text-align:left; vertical-align: middle;' class='text-capitalize'>";echo $row['alsw'];"</td>";

                                                //Tombol aksi
                                                echo "<td style='width:110px;  text-align:left; vertical-align: middle;' class='text-capitalize'><button id='detilsiswa' class='btn btn-info' data-toggle='modal' href='#' data-target='#detilsiswaModal".$row['nis']."' title='Lihat Detil'><i class='fa fa-eye'></i></button>
                                                </td>";
                                                //End tombol aksi

                                            echo '</tr>';
                                    }
                                    echo '</tbody>';
                                echo '</table>';
                            ?>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->