<?php
	$hasil = $db->query("SELECT a.*, b.nama_kelas from guru a, kelas b where a.nip = b.nip and a.nip='$_SESSION[guru]'");

    $r = $hasil->fetch(PDO::FETCH_ASSOC);
?>
<div class="row" >
    <div class="col-lg-12">
    	<div class="panel panel-primary">
    	  	<div class="panel-body">
    	  	<div class="col-lg-10">
    	  	<div class="row">
    			<div class="col-lg-5">
			    	<dl class="dl">
			    	  <dt>Nama lengkap</dt>
			    	  <dd><?php echo $r['nama']?> - <b style="text-transform:uppercase"><?php echo $r['nip']?></b></dd><br>
			    	  <dt>Wali Kelas</dt>
			    	  <dd><b style="text-transform:uppercase"><?php echo $r['nama_kelas']?></b></dd><br>
			    	</dl>
			    	<hr>
	    	  	</div>
	    	  	<div class="col-lg-5">
			    	<dl class="dl">
			    	  <dt>Jenis kelamin</dt>
			    	  <dd><?php echo $r['jenkel']?></dd><br>
			    	  <dt>Nomor telepon</dt>
			    	  <dd><?php echo $r['notelp']?></dd><br>
			    	  <dt>Alamat</dt>
			    	  <dd><?php echo $r['alamat']?></dd><br>
			    	</dl>
			    	<hr>
	    	  	</div>
	    	  	<div class="col-lg-5">
			    	<dl class="dl">
			    	  <dt>Tempat lahir</dt>
			    	  <dd><?php echo $r['tempat_lahir']?></dd><br>
			    	  <dt>Tanggal lahir</dt>
			    	  <dd><?php echo $r['tanggal_lahir']?></dd><br>
			    	</dl>
			    	<hr>
	    	  	</div>
	    	</div>
	    	</div>
	    	<div class="col-md-2">
	    	<div class="row">
	    		<div class="col-lg-2">
	    			<img src="../pages/mod_guru/foto/<?php echo $r['foto'];?>" alt="foto-pegawai" width="150" height="200"  class="img-rounded" onmousedown="return false" oncontexmenu="return false" onselectstart="return false"/>
	    		</div>
	    	</div>
	    	</div>
	    </div>
    </div>
</div>
<!-- *************************************End 