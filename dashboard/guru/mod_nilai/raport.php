            <div class="panel panel-default">
              <div class="panel-body">
                <?php
                  $hasil = $db->query("SELECT nama_kelas from guru a, kelas b where a.nip = b.nip and a.nip='$_SESSION[guru]'");

                  $tampil = $hasil->fetch(PDO::FETCH_ASSOC);
                ?>
                <h4><i class="fa fa-file fa-fw"></i>Cetak Raport Kelas <b style="text-transform: uppercase;"><?php echo $tampil['nama_kelas'];?></b></h4>
              </div>
            </div>
            <div class="panel panel-default">
              <div class="panel-body">
                <form action="mod_nilai/proses.php" method="post">
                <div class="form-group"><label>Semester</label>
                        <select class="form-control" name="smt" required>
                            <option value="Ganjil">1 - Ganjil</option>
                            <option value="Genap">2 - Genap</option>
                        </select>
                </div>
                <div><button type="submit" class="btn btn-success pull-center" name="tampilraport" width="100%">Tampilkan</button> <p class="help-block pull-left text-danger hide" id="form-error">&nbsp; The form is not valid. </p></div>
                </form>
              </div>
            </div>
            <?php
                if(!empty($_GET['semester'])){
            ?>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <?php
                                include('../connection/connection.php');
                                $stmt = $db->query("SELECT a.nis, a.nama as nmsw, b.*, c.nip from siswa a, nilai b, guru c where a.nis=b.nis and b.nip=c.nip
                                                    and c.nip='$_SESSION[guru]' and b.semester = '$_GET[semester]' group by a.nis");
                                echo'<table style="table-layout:fixed;" class="table table-striped table-bordered table-hover" id="dataTables-example">';
                                    echo '<thead>';
                                        echo '<tr>';
                                            echo '<th>NIS</th>';
                                            echo '<th>Nama Murid</th>';
                                            echo '<th>Semester</th>';
                                            echo '<th>Aksi</th>';
                                        echo '</tr>';
                                    echo '</thead>';
                                    echo '<tbody>';
                                    while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                                            echo "<tr'>";
                                                echo "<form method='POST' action='mod_nilai/proses.php'>";       
                                                echo "<td style=' width:150px;  text-align:left; padding: 10px;vertical-align: middle;' class='text-uppercase'>";echo $row['nis'];echo"</td>";
                                                echo "<td style='width:110px;  text-align:left; vertical-align: middle;' class='text-capitalize'>";echo $row['nmsw'];"</td>";
                                                echo "<td style='width:110px;  text-align:left; vertical-align: middle;' class='text-capitalize'>";echo $row['semester'];"</td>";

                                                //Tombol aksi
                                                echo "<td style='width:110px;  text-align:left; vertical-align: middle;' class='text-capitalize'>
                                                        <a target='_blank' title='Cetak Raport' style='margin-top:5px' class='btn btn-info' href='data/raport.php?nis=$row[nis]&&semester=$row[semester]&&nip=$_SESSION[guru]' ><i class='fa fa-print'></i></a>
                                                      </td>";
                                                //End tombol aksi

                                            echo '</tr>';
                                    }
                                    echo '</tbody>';
                                echo '</table>';
                            ?>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <?php
                }
            ?>