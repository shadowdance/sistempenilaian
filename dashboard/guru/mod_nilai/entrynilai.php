        <div class="panel panel-default">
          <div class="panel-body">
            <i class="fa fa-pencil fa-fw"></i>Entry Nilai | <b><a href="index.php?hal=dn">Lihat nilai disini</a></b>
          </div>
        </div>
        <div class="row">
                <div class="col-lg-12">
                    <?php
                        if (isset($_GET['tmb'])) {
                            if($_GET['tmb']=="success") {
                                ?>
                                <div class="alert alert-success" id="success-alert-input">
                                    Data berhasil disimpan.
                                </div>
                            <?php }else{ ?>
                                <div class="alert alert-danger" id="fail-alert-input">
                                    Data gagal disimpan, nilai sudah pernah di input.
                                </div>
                            <?php }
                        }
                        else if (isset($_GET['ubh'])) {
                                if($_GET['ubh']=="success") {
                                    ?>
                                    <div class="alert alert-success" id="success-alert-edit">
                                        Data berhasil diedit.
                                    </div>
                                <?php }else{ ?>
                                    <div class="alert alert-danger" id="fail-alert-edit">
                                        Data gagal diedit.
                                    </div>
                                <?php }
                        }
                        else if (isset($_GET['hps'])) {
                                if($_GET['hps']=="success") {
                                    ?>
                                    <div class="alert alert-success" id="success-alert-delete">
                                        Data berhasil dihapus.
                                    </div>
                                <?php }else{ ?>
                                    <div class="alert alert-danger" id="fail-alert-delete">
                                        Data gagal dihapus.
                                    </div>
                                <?php }
                        }
                    ?>
                </div>
        </div>
        <div class="row">
          <div class="col-lg-12">
            <div class="panel panel-default">
              <!-- /.panel-heading -->
              <div class="panel-body">
                <form method="POST" action="mod_nilai/proses.php" enctype="multipart/form-data" class="form-horizontal">
                  <div class="form-group"><label class="col-sm-2">Semester</label>
                    <div class="form-group col-md-8">
                        <select class="form-control" name="smt" required>
                            <option value="Ganjil">1 - Ganjil</option>
                            <option value="Genap">2 - Genap</option>
                        </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-2">Pelajaran</label>
                    <div class="form-group text-capitalize col-md-8">
                      <select class="form-control text-uppercase" style="width: 100%;" name="mapel" required>
                          <?php
                              $query = $db->query("SELECT * from mata_pelajaran");

                              while ($row = $query->fetch(PDO::FETCH_ASSOC)){
                              echo "<option value=$row[kd_mapel]>$row[nama_mapel]</option>";
                              }
                          ?>  
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-2">Pilih Murid</label>
                    <div class="form-group text-capitalize col-md-8">
                      <select class="form-control select2 text-uppercase" style="width: 100%;" name="nis" required>
                          <?php
                              $query = $db->query("SELECT a.nama as nmsw, a.nis, b.nip as nipgr, c.* from siswa a, guru b, kelas c 
                                                   where a.kd_kelas = c.kd_kelas and c.nip = b.nip and b.nip='$_SESSION[guru]' order by a.nama asc");

                              $nourut = 1;
                              while ($row = $query->fetch(PDO::FETCH_ASSOC)){
                              echo "<option value=$row[nis]>$nourut | $row[nmsw]</option>";
                              $nourut++;
                              }
                          ?>  
                      </select>
                    </div>
                  </div>
                  <div class="form-group"><label class="col-md-2">Nilai Tugas</label><div class="form-group col-md-8"><input required class="form-control" placeholder="Input Nilai Tugas" data-placement="top" data-trigger="manual" type="text" name="ntugas" id="tgsinp" maxlength="3"></div></div>
                  <div class="form-group"><label class="col-md-2">Nilai UTS</label><div class="form-group col-md-8"><input required class="form-control" placeholder="Input Nilai UTS" data-placement="top" data-trigger="manual" type="text" name="nuts" id="utsinp" maxlength="3"></div></div>
                  <div class="form-group"><label class="col-md-2">Nilai UAS</label><div class="form-group col-md-8"><input required class="form-control" placeholder="Input Nilai UAS" data-placement="top" data-trigger="manual" type="text" name="nuas" id="uasinp" maxlength="3"></div></div>
                  <div class="form-group"><label class="col-md-2">Keterangan</label>
                    <div class="form-group col-md-8">
                      <textarea class="form-control" name="keterangan" required></textarea>
                    </div>
                  </div>
                  <input type="hidden" name="nip" value="<?php echo $_SESSION['guru']; ?>">
                  <div class="form-group col-md-8"><button type="submit" class="btn btn-success pull-center" name="tambah" width="100%">Submit</button> <p class="help-block pull-left text-danger hide" id="form-error">&nbsp; The form is not valid. </p></div>
                </form>
              </div>
            </div>
          </div>
        </div>