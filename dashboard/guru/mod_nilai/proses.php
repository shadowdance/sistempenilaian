<?php
	include('../../connection/connection.php');
	$smt = $_POST['smt'];
	$mapel = $_POST['mapel'];
	$nis = $_POST['nis'];
	$ntugas = $_POST['ntugas'];
	$nuts = $_POST['nuts'];
	$nuas = $_POST['nuas'];
	$keterangan = $_POST['keterangan'];
	$nip = $_POST['nip'];

	$nilakhir = $ntugas + $nuts + $nuas;
	$nilakhirfix = $nilakhir/3;

	if(isset($_POST['tambah'])){
		$sql = $db->query("SELECT nis, kd_mapel, semester, guru.nip as nipguru from nilai, guru");
		$ada = $sql->fetch(PDO::FETCH_ASSOC);

		if($ada['nis']==$nis && $ada['kd_mapel']==$mapel && $ada['semester']==$smt && $ada['nipguru']==$nip){ // Cek apakah pernah ada nilai yg diinput di mapel dan smt yg sama
            // Jika data sudah ada sebelumnya, Lakukan :
            header("location: ../index.php?hal=en&tmb=fail"); // Redirect ke halaman index.php
        }else{
            // Jika data belum ada, Lakukan input data :
            $sql = $db->exec("INSERT into nilai (kd_nilai, kd_mapel, nis, nil_tugas, nil_uts, nil_uas, keterangan, semester, nil_akhir, nip) 
						  values(NULL, '$mapel', '$nis', '$ntugas', '$nuts', '$nuas', '$keterangan', '$smt', '$nilakhirfix', '$nip')");
            header("location: ../index.php?hal=en&tmb=success");

        }
	}else if(isset($_POST['update'])){
        $kd_nilaie = $_POST['kd_nilai'];
        // Proses ubah data ke Database
        $sql = $db->exec("UPDATE nilai SET nil_tugas='$ntugas', nil_uts='$nuts', nil_uas='$nuas', nil_akhir='$nilakhirfix' where kd_nilai='$kd_nilaie'");

        if($sql){ // Cek jika proses simpan ke database sukses atau tidak
            // Jika Sukses, Lakukan :
            header('location:'.$_SERVER['HTTP_REFERER'].'&&ubh=success'); // Redirect ke halaman
        }else{
            // Jika Gagal, Lakukan :
            die ("sql gagal");
        }
	}else if(isset($_POST['hapus'])){
        $kd_nilaih = $_POST['kd_nilai'];

        // query SQL untuk delete data
        $sql = $db->exec("DELETE from nilai where kd_nilai='$kd_nilaih'");

        if($sql){ // Cek jika proses simpan ke database sukses atau tidak
            // Jika Sukses, Lakukan :
            header('location:'.$_SERVER['HTTP_REFERER'].'&&hps=success'); // Redirect ke halaman
        }else{
            // Jika Gagal, Lakukan :
            die ("sql gagal");
        }
    }else if(isset($_POST['tampil'])){
		header("location: ../index.php?hal=dn&&mapel=$mapel&&semester=$smt"); // Redirect ke halaman
    }else if(isset($_POST['tampilraport'])){
		header("location: ../index.php?hal=cr&&semester=$smt"); // Redirect ke halaman
    }
?>