        <?php
          include('../connection/connection.php'); 
          //$id_bagian = $_POST['id_bagian'];
          
          $stmt = $db->query("select a.*, b.*, c.nis as nissw, c.nama from nilai a, mata_pelajaran b, siswa c where a.nis=c.nis and a.kd_mapel=b.kd_mapel");
        
          //<!-- update bagian modal -->
          while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) { 
        ?>
        <div <?php echo 'id="updatenilaiModal'.$row['kd_nilai'].'"' ?> class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3 id="myModalLabel">Update Nilai</h3>
              </div>
              <div class="modal-body">
                <form method="POST" action="mod_nilai/proses.php" enctype="multipart/form-data">
                  <input type="hidden" value="<?php echo $row['kd_nilai']; ?>" name="kd_nilai">
                  <div class="form-group"><label>NIS</label><input class="form-control required text-uppercase" placeholder="Input ID nilai" data-placement="top" data-trigger="manual" name="nis" type="text" value="<?php echo $row['nis']; ?>" readonly></div>
                  <div class="form-group"><label>Nama Murid</label><input class="form-control required text-uppercase" placeholder="Input ID nilai" data-placement="top" data-trigger="manual" type="text" value="<?php echo $row['nama']; ?>" readonly></div>
                  <div class="form-group"><label>Pelajaran</label><input class="form-control required text-uppercase" placeholder="Input ID nilai" data-placement="top" data-trigger="manual" type="text" value="<?php echo $row['nama_mapel']; ?>" readonly></div>
                  <div class="form-group"><label>Semester</label><input class="form-control required text-uppercase" placeholder="Input ID nilai" data-placement="top" data-trigger="manual" type="text" value="<?php echo $row['semester']; ?>" readonly></div>
                  <div class="form-group"><label>Nilai Tugas</label><input required class="form-control required text-uppercase" placeholder="Input Nilai Tugas" data-placement="top" data-trigger="manual" type="number" name="ntugas" value="<?php echo $row['nil_tugas']; ?>"></div>
                  <div class="form-group"><label>Nilai UTS</label><input required class="form-control required text-uppercase" placeholder="Input Nilai UTS" data-placement="top" data-trigger="manual" type="number" name="nuts" value="<?php echo $row['nil_uts']; ?>"></div>
                  <div class="form-group"><label>Nilai UAS</label><input required class="form-control required text-uppercase" placeholder="Input Nilai UAS" data-placement="top" data-trigger="manual" type="number" name="nuas" value="<?php echo $row['nil_uas']; ?>"></div>
                  <div class="form-group"><button type="submit" class="btn btn-success pull-center" name="update">Update</button> <p class="help-block pull-left text-danger hide" id="form-error">&nbsp; The form is not valid. </p></div>
                </form>
              </div>
            </div>
          </div>
        </div>
        <?php 
          } 
        ?>
        <!-- /.update bagian modal -->