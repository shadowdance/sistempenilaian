            <div class="panel panel-default">
              <div class="panel-body">
                <?php
                  $hasil = $db->query("SELECT nama_kelas from guru a, kelas b where a.nip = b.nip and a.nip='$_SESSION[guru]'");

                  $tampil = $hasil->fetch(PDO::FETCH_ASSOC);
                ?>
                <h4><i class="fa fa-clipboard fa-fw"></i>Data nilai kelas <b style="text-transform: uppercase;"><?php echo $tampil['nama_kelas'];?></b></h4>
              </div>
            </div>
            <div class="panel panel-default">
              <div class="panel-body">
                <form action="mod_nilai/proses.php" method="post">
                <div class="form-group"><label>Semester</label>
                        <select class="form-control" name="smt" required>
                            <option value="Ganjil">1 - Ganjil</option>
                            <option value="Genap">2 - Genap</option>
                        </select>
                </div>
                <div class="form-group">
                    <label>Pelajaran</label>
                      <select class="form-control text-uppercase" style="width: 100%;" name="mapel" required>
                          <?php
                              $query = $db->query("SELECT * from mata_pelajaran");

                              while ($row = $query->fetch(PDO::FETCH_ASSOC)){
                              echo "<option value=$row[kd_mapel]>$row[nama_mapel]</option>";
                              }
                          ?>  
                      </select>
                </div>
                <div><button type="submit" class="btn btn-success pull-center" name="tampil" width="100%">Tampilkan</button> <p class="help-block pull-left text-danger hide" id="form-error">&nbsp; The form is not valid. </p></div>
                </form>
              </div>
            </div>
            <?php
                if(!empty($_GET['mapel']) &&  !empty($_GET['semester'])){
            ?>
            <div class="row">
                <div class="col-lg-12">
                    <?php
                        if (isset($_GET['tmb'])) {
                            if($_GET['tmb']=="success") {
                                ?>
                                <div class="alert alert-success" id="success-alert-input">
                                    Data berhasil disimpan.
                                </div>
                            <?php }else{ ?>
                                <div class="alert alert-danger" id="fail-alert-input">
                                    Data gagal disimpan, nilai sudah pernah di input.
                                </div>
                            <?php }
                        }
                        else if (isset($_GET['ubh'])) {
                                if($_GET['ubh']=="success") {
                                    ?>
                                    <div class="alert alert-success" id="success-alert-edit">
                                        Data berhasil diedit.
                                    </div>
                                <?php }else{ ?>
                                    <div class="alert alert-danger" id="fail-alert-edit">
                                        Data gagal diedit.
                                    </div>
                                <?php }
                        }
                        else if (isset($_GET['hps'])) {
                                if($_GET['hps']=="success") {
                                    ?>
                                    <div class="alert alert-success" id="success-alert-delete">
                                        Data berhasil dihapus.
                                    </div>
                                <?php }else{ ?>
                                    <div class="alert alert-danger" id="fail-alert-delete">
                                        Data gagal dihapus.
                                    </div>
                                <?php }
                        }
                    ?>
                </div>
        </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <?php
                                $query = $db->query("SELECT a.*, b.* from mata_pelajaran a, nilai b, guru c where a.kd_mapel=b.kd_mapel
                                                     and a.kd_mapel='$_GET[mapel]'");
                                $tampil2 = $query->fetch(PDO::FETCH_ASSOC);
                            ?>
                            Data nilai Mata Pelajaran <b style="text-transform:uppercase;"><?php echo $tampil2['nama_mapel'] ?></b>
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <?php
                                include('../connection/connection.php');
                                $stmt = $db->query("SELECT a.nis, a.nama as nmsw, b.*, c.nip from siswa a, nilai b, guru c where a.nis=b.nis and b.nip=c.nip
                                                    and c.nip='$_SESSION[guru]' and b.kd_mapel = '$_GET[mapel]' and b.semester = '$_GET[semester]'");
                                echo'<table style="table-layout:fixed;" class="table table-striped table-bordered table-hover" id="dataTables-example">';
                                    echo '<thead>';
                                        echo '<tr>';
                                            echo '<th>NIS</th>';
                                            echo '<th>Nama Murid</th>';
                                            echo '<th>Semester</th>';
                                            echo '<th>Nilai Tugas</th>';
                                            echo '<th>Nilai UTS</th>';
                                            echo '<th>Nilai UAS</th>';
                                            echo '<th>Aksi</th>';
                                        echo '</tr>';
                                    echo '</thead>';
                                    echo '<tbody>';
                                    while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                                            echo "<tr'>";
                                                echo "<form method='POST' action='mod_nilai/proses.php'>";       
                                                echo "<td style=' width:150px;  text-align:left; padding: 10px;vertical-align: middle;' class='text-uppercase'>";echo $row['nis'];echo"</td>";
                                                echo "<td style='width:110px;  text-align:left; vertical-align: middle;' class='text-capitalize'>";echo $row['nmsw'];"</td>";
                                                echo "<td style='width:110px;  text-align:left; vertical-align: middle;' class='text-capitalize'>";echo $row['semester'];"</td>";
                                                echo "<td style='width:110px;  text-align:left; vertical-align: middle;' class='text-capitalize'>";echo $row['nil_tugas'];"</td>";
                                                echo "<td style='width:110px;  text-align:left; vertical-align: middle;' class='text-capitalize'>";echo $row['nil_uts'];"</td>";
                                                echo "<td style='width:110px;  text-align:left; vertical-align: middle;' class='text-capitalize'>";echo $row['nil_uas'];"</td>";

                                                //Tombol aksi
                                                echo "<td style='width:110px;  text-align:left; vertical-align: middle;' class='text-capitalize'><button id='updateguru' class='btn btn-primary' data-toggle='modal' href='#' data-target='#updatenilaiModal".$row['kd_nilai']."' title='Edit'><i class='fa fa-edit'></i></button>&nbsp;<button title='Delete' class='btn btn-danger' href='#' data-toggle='modal' data-target='#deleteModal".$row['kd_nilai']."'><i class='fa fa-remove'></i></button>
                                                </td>";
                                                //End tombol aksi
                                                //MODAL DELETE
                                                echo '<div class="modal fade" id="deleteModal'.$row["kd_nilai"].'" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">';
                                                echo '<div class="modal-dialog modal-sm" role="document">';
                                                echo '<div class="modal-content">';
                                                echo '<div class="modal-header">
                                                    <button class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                    <h4 class="modal-title" id="myModalLabel">Confirm Delete</h4>
                                                </div>';
                                                echo "<div class='modal-body'>Kamu yakin ingin menghapus?
                                                </div>
                                                    <div class='modal-footer'>
                                                        <input type='hidden' value='$row[kd_nilai]' name='kd_nilai'>
                                                        <button type='button' class='btn btn-default' data-dismiss='modal'>Batal</button>
                                                        <button class='btn btn-danger' aria-label='Delete'type='submit' name='hapus'></span>Hapus</button>
                                                    </div>
                                                </div>";
                                                echo "</form>";
                                                //-END MODAL DELETE

                                            echo '</tr>';
                                    }
                                    echo '</tbody>';
                                echo '</table>';
                            ?>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <?php
                }
            ?>