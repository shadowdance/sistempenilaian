<div class="panel panel-default">
  <div class="panel-body">
    <center>
    <table class="table table-responsive" border="0">
      <tr>
        <td width="100" height="100"><img src="../../img/logo.jpg" width="100" height="100"></td>
        <td><img src="../../img/head.jpg" height="100"></td>
      </tr>
    </table>
    </center>
  </div>
</div>

<div class="row">
  <div class="col-lg-12">
    <div class="panel panel-default">
      <div class="panel-body">
      <div class="row" style="margin-top: 20px;">
      <!-- PANEL -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-blue">
            <div class="inner">
              <h3>
                <?php
                    include('../connection/connection.php');
                    $stmt = $db->query('SELECT COUNT(nip) as total from guru');
                    $row = $stmt->fetch(PDO::FETCH_ASSOC);

                    echo $row['total'];
                ?>
              </h3>

              <p>Guru</p>
            </div>
            <div class="icon" style="color: white">
              <i class="fa fa-user"></i>
            </div>
            <a href="index.php?hal=gr" class="small-box-footer">Lihat Selengkapnya <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <!-- PANEL -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3>
                <?php
                    include('../connection/connection.php');
                    $stmt = $db->query('SELECT COUNT(nis) as total from siswa');
                    $row = $stmt->fetch(PDO::FETCH_ASSOC);

                    echo $row['total'];
                ?>
              </h3>

              <p>Siswa</p>
            </div>
            <div class="icon" style="color: white">
              <i class="fa fa-users"></i>
            </div>
            <a href="index.php?hal=sw" class="small-box-footer">Lihat Selengkapnya <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3>
                <?php
                    include('../connection/connection.php');
                    $stmt = $db->query('SELECT COUNT(kd_mapel) as total from mata_pelajaran');
                    $row = $stmt->fetch(PDO::FETCH_ASSOC);

                    echo $row['total'];
                ?>
              </h3>

              <p>Mata Pelajaran</p>
            </div>
            <div class="icon" style="color: white">
              <i class="fa fa-book"></i>
            </div>
            <a href="index.php?hal=mp" class="small-box-footer">Lihat Selengkapnya <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3>
                <?php
                    include('../connection/connection.php');
                    $stmt = $db->query('SELECT COUNT(kd_kelas) as total from kelas');
                    $row = $stmt->fetch(PDO::FETCH_ASSOC);

                    echo $row['total'];
                ?>
              </h3>

              <p>Kelas</p>
            </div>
            <div class="icon" style="color: white">
              <i class="fa fa-cube"></i>
            </div>
            <a href="index.php?hal=kl" class="small-box-footer">Lihat Selengkapnya <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
      </div>
      </div>
    </div>
  </div>
</div>