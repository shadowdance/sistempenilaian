        <!-- entry kelas modal -->
        <div id="entrykelasModal" class="modal fade" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3 id="myModalLabel">Tambah Kelas</h3>
              </div>
              <div class="modal-body">
                <form method="POST" action="mod_kelas/proses.php" enctype="multipart/form-data">
                  <div class="form-group"><label>Nama Kelas</label><input required class="form-control text-uppercase" placeholder="Input Nama Kelas" data-placement="top" data-trigger="manual" type="text" name="namakelas" maxlength="2"></div>
                  <div class="form-group">
                    <label>Wali Kelas</label>
                    <div class="form-group text-capitalize">
                      <select class="form-control select2 text-capitalize" name="nip" data-width="100%" required>
                          <option value="">--Pilih Wali Kelas--</option>
                          <?php
                              include('../connection/connection.php');
                              $query = $db->query("SELECT nip, nama from guru");

                              while ($row = $query->fetch(PDO::FETCH_ASSOC)){
                              echo "<option value=$row[nip]>$row[nip] - $row[nama]</option>";
                              }
                          ?>
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label>Tahun Ajaran</label>
                    <div class="form-group text-capitalize">
                      <select class="form-control text-capitalize" style="width: 100%;" name="thajar" required>
                          <option value="">--Pilih Tahun Ajaran--</option>
                          <?php
                              include('../connection/connection.php');
                              $query = $db->query("SELECT * from tahun_ajaran");

                              while ($row = $query->fetch(PDO::FETCH_ASSOC)){
                              echo "<option value=$row[kd_ta]>$row[tahun_ajaran]</option>";
                              }
                          ?>
                      </select>
                    </div>
                  </div>
                  <div class="form-group"><button type="submit" class="btn btn-success pull-center" name="tambah">Submit</button> <p class="help-block pull-left text-danger hide" id="form-error">&nbsp; The form is not valid. </p></div>
                </form>
              </div>
            </div>
          </div>
        </div>
        <!-- /.entry kelas modal -->
