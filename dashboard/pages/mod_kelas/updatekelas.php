        <?php
          include('../connection/connection.php'); 
          //$id_bagian = $_POST['id_bagian'];
          
          $stmt = $db->query("select a.*, b.nip as nipguru, c.kd_ta as tarl from kelas a, guru b, tahun_ajaran c where b.nip=a.nip and a.kd_ta = c.kd_ta");

          //Fungsi Cek\
          class selectedguru{
              function cek($val,$sel,$tipe){
                  if($val==$sel){
                      switch($tipe){
                          case 'select' :echo "selected"; break;
                          case 'radio' :echo "checked"; break;
                      }
                  }else{
                      echo "";
                  }
              }
          }
          $ob = new selectedguru();
        
          //<!-- update bagian modal -->
          while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) { 
        ?>
        <div <?php echo 'id="updatekelasModal'.$row['kd_kelas'].'"' ?> class="modal fade" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3 id="myModalLabel">Update Kelas</h3>
              </div>
              <div class="modal-body">
                <form method="POST" action="mod_kelas/proses.php" enctype="multipart/form-data">
                  <div class="form-group"><label>Kode Kelas</label><input class="form-control required text-uppercase" placeholder="Input ID Kelas" data-placement="top" data-trigger="manual" type="text" name="kd_kelas" maxlength="2" value="<?php echo $row['kd_kelas']; ?>" readonly></div>
                  <div class="form-group"><label>Nama Kelas</label><input required class="form-control required text-uppercase" placeholder="Input Nama" data-placement="top" data-trigger="manual" maxlength="2" type="text" name="namakelas" value="<?php echo $row['nama_kelas']; ?>"></div>
                  <div class="form-group">
                      <label>Wali Kelas</label>
                        <select class="form-control select2" style="width:100%;" name="nip">
                            <?php
                                include('../connection/connection.php'); 
                                $hasil = $db->query("select nip,nama from guru");
                                while($row2 = $hasil->fetch(PDO::FETCH_ASSOC)){
                                ?>
                                  <option <?php $ob->cek($row2['nip'],$row['nipguru'],"select") ?> value="<?php echo $row2['nip']?>"> <?php echo $row2['nama']?> </option>;
                                <?php
                                }
                            ?>
                        </select>
                  </div>
                  <div class="form-group">
                      <label>Tahun Ajaran</label>
                        <select class="form-control text-capitalize" style="width:100%;" name="thajar">
                            <?php
                                include('../connection/connection.php'); 
                                $hasil = $db->query("select * from tahun_ajaran");
                                while($row3 = $hasil->fetch(PDO::FETCH_ASSOC)){
                                ?>
                                  <option <?php $ob->cek($row3['kd_ta'],$row['tarl'],"select") ?> value="<?php echo $row3['kd_ta']?>"> <?php echo $row3['tahun_ajaran']?> </option>;
                                <?php
                                }
                            ?>
                        </select>
                  </div>
                  <div class="form-group"><button type="submit" class="btn btn-success pull-center" name="update">Update</button> <p class="help-block pull-left text-danger hide" id="form-error">&nbsp; The form is not valid. </p></div>
                </form>
              </div>
            </div>
          </div>
        </div>
        <?php 
          } 
        ?>
        <!-- /.update bagian modal -->