            <div class="panel panel-default">
              <div class="panel-body"><h4><i class="fa fa-cube fa-fw"></i>Data Kelas</h4></div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <?php
                        if (isset($_GET['tmb'])) {
                            if($_GET['tmb']=="success") {
                                ?>
                                <div class="alert alert-success" id="success-alert-input">
                                    Data berhasil disimpan.
                                </div>
                            <?php }else{ ?>
                                <div class="alert alert-danger" id="fail-alert-input">
                                    Data gagal disimpan.
                                </div>
                            <?php }
                        }
                        else if (isset($_GET['ubh'])) {
                                if($_GET['ubh']=="success") {
                                    ?>
                                    <div class="alert alert-success" id="success-alert-edit">
                                        Data berhasil diedit.
                                    </div>
                                <?php }else{ ?>
                                    <div class="alert alert-danger" id="fail-alert-edit">
                                        Data gagal diedit.
                                    </div>
                                <?php }
                        }
                        else if (isset($_GET['hps'])) {
                                if($_GET['hps']=="success") {
                                    ?>
                                    <div class="alert alert-success" id="success-alert-delete">
                                        Data berhasil dihapus.
                                    </div>
                                <?php }else{ ?>
                                    <div class="alert alert-danger" id="fail-alert-delete">
                                        Data gagal dihapus.
                                    </div>
                                <?php }
                        }
                    ?>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <button class="btn btn-default" data-toggle="modal" href="#" data-target="#entrykelasModal"><i class="fa fa-plus"></i></button> Tambah Data Baru
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <?php
                                include('../connection/connection.php');
                                $stmt = $db->query("SELECT a.*, b.nama, c.tahun_ajaran FROM kelas a, guru b, tahun_ajaran c where a.nip = b.nip and a.kd_ta = c.kd_ta");
                                echo'<table style="table-layout:fixed;" class="table table-striped table-bordered table-hover" id="dataTables-example">';
                                    echo '<thead>';
                                        echo '<tr>';
                                            echo '<th>Kode Kelas</th>';
                                            echo '<th>Nama Kelas</th>';
                                            echo '<th>Wali Kelas</th>';
                                            echo '<th>Tahun Ajaran</th>';
                                            echo '<th>Aksi</th>';
                                        echo '</tr>';
                                    echo '</thead>';
                                    echo '<tbody>';
                                    while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                                            echo "<tr'>";
                                                echo "<form method='POST' action='mod_kelas/proses.php'>";       
                                                echo "<td style=' width:150px;  text-align:left; padding: 10px;vertical-align: middle;' class='text-uppercase'>";echo $row['kd_kelas'];echo"</td>";
                                                echo "<td style='width:110px;  text-align:left; vertical-align: middle;' class='text-uppercase'>";echo $row['nama_kelas'];"</td>";
                                                echo "<td style='width:110px;  text-align:left; vertical-align: middle;' class='text-capitalize'>";echo $row['nama'];"</td>";
                                                echo "<td style='width:110px;  text-align:left; vertical-align: middle;' class='text-capitalize'>";echo $row['tahun_ajaran'];"</td>";

                                                //Tombol aksi
                                                echo "<td style='width:110px;  text-align:left; vertical-align: middle;' class='text-capitalize'><button id='updateguru' class='btn btn-primary' data-toggle='modal' href='#' data-target='#updatekelasModal".$row['kd_kelas']."' title='Edit'><i class='fa fa-edit'></i></button>&nbsp;<button title='Delete' class='btn btn-danger' href='#' data-toggle='modal' data-target='#deleteModal".$row['kd_kelas']."'><i class='fa fa-remove'></i></button>
                                                </td>";
                                                //End tombol aksi
                                                //MODAL DELETE
                                                echo '<div class="modal fade" id="deleteModal'.$row["kd_kelas"].'" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">';
                                                echo '<div class="modal-dialog modal-sm" role="document">';
                                                echo '<div class="modal-content">';
                                                echo '<div class="modal-header">
                                                    <button class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                    <h4 class="modal-title" id="myModalLabel">Confirm Delete</h4>
                                                </div>';
                                                echo "<div class='modal-body'>Kamu yakin ingin menghapus?
                                                </div>
                                                    <div class='modal-footer'>
                                                        <input type='hidden' value='$row[kd_kelas]' name='kd_kelas'>
                                                        <button type='button' class='btn btn-default' data-dismiss='modal'>Batal</button>
                                                        <button class='btn btn-danger' aria-label='Delete'type='submit' name='hapus'></span>Hapus</button>
                                                    </div>
                                                </div>";
                                                echo "</form>";
                                                //-END MODAL DELETE

                                            echo '</tr>';
                                    }
                                    echo '</tbody>';
                                echo '</table>';
                            ?>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->