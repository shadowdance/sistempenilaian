        <!-- entry siswa modal -->
        <div id="entrysiswaModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3 id="myModalLabel">Tambah Siswa</h3>
              </div>
              <div class="modal-body">
                <form method="POST" action="mod_siswa/proses.php" enctype="multipart/form-data">
                  <div class="form-group"><label>Foto (maks 2mb)</label><input required class="form-control" data-placement="top" data-trigger="manual" type="file" name="foto"></div>
                  <div class="form-group"><label>Nama</label><input required class="form-control required text-capitalize" placeholder="Input Nama Siswa" data-placement="top" data-trigger="manual" type="text" name="nama"></div>
                  <div class="form-group"><label>Jenis Kelamin</label>
                    <div class="selectContainer">
                        <select class="form-control" name="jenkel">
                            <option value="Laki-laki">Laki-laki</option>
                            <option value="Perempuan">Perempuan</option>
                        </select>
                    </div>
                  </div>
                  <div class="form-group"><label>Tempat Lahir</label><input required class="form-control required text-capitalize" placeholder="Input Tempat Lahir" data-placement="top" data-trigger="manual" type="text" name="tmplahir"></div>
                  <div class="form-group"><label>Tanggal Lahir</label><input required class="form-control" placeholder="yyyy-mm-dd" data-placement="top" data-trigger="manual" type="date" name="tgllahir"></div>
                  <div class="form-group"><label>Agama</label>
                    <div class="selectContainer">
                        <select class="form-control" name="agama">
                            <option value="Islam">Islam</option>
                            <option value="Protestan">Protestan</option>
                            <option value="Katholik">Katholik</option>
                            <option value="Hindu">Hindu</option>
                            <option value="Buddha">Buddha</option>
                            <option value="Konghuchu">Konghuchu</option>
                        </select>
                    </div>
                  </div>
                  <div class="form-group"><label>Alamat</label>
                    <textarea class="form-control" name="alamat" required></textarea>
                  </div>
                  <div class="form-group"><label>Nomor Telepon</label><input required class="form-control required" placeholder="xxxxxxxxxxxxx" data-placement="top" data-trigger="manual" type="text" name="notelp" id="notelpsiswa" maxlength="13"></div>
                  <div class="form-group">
                    <label>Pilih Kelas</label>
                    <div class="form-group text-capitalize">
                      <select class="form-control text-uppercase" style="width: 100%;" name="kelas" required>
                          <option value="">--Pilih Kelas--</option>
                          <?php
                              include('../connection/connection.php');
                              $query = $db->query("SELECT * from kelas a, tahun_ajaran b where a.kd_ta = b.kd_ta");

                              while ($row = $query->fetch(PDO::FETCH_ASSOC)){
                              echo "<option value=$row[kd_kelas]>$row[nama_kelas] | $row[tahun_ajaran]</option>";
                              }
                          ?>  
                      </select>
                    </div>
                  </div>
                  <div class="form-group"><button type="submit" class="btn btn-success pull-center" name="tambah">Submit</button> <p class="help-block pull-left text-danger hide" id="form-error">&nbsp; The form is not valid. </p></div>
                </form>
              </div>
            </div>
          </div>
        </div>
        <!-- /.entry siswa modal -->