<?php
	include('../../connection/connection.php');
    $nama = $_POST['nama'];
    $alamat = $_POST['alamat'];
    $notelp = $_POST['notelp'];
    $jenkel = $_POST['jenkel'];
    $tmplahir = $_POST['tmplahir'];
    $tgllahir = $_POST['tgllahir'];
    $agama = $_POST['agama'];
    $kelas = $_POST['kelas'];

    //cari id terbesar
    $stmt = $db->query('SELECT max(nis) as maxNO FROM siswa');
    $row = $stmt->fetch(PDO::FETCH_ASSOC);
    $idMax = $row['maxNO']; //0
    //setelah membaca id terbesar, lanjut mencari nomor urut id dari id terbesar
    $NoUrut = (int) substr($idMax, 4, 4);
    $NoUrut++; //nomor urut +1
    $kode = date("md");
    $rand = rand(11, 99);

  //PROSES TAMBAH DATA
  if(isset($_POST['tambah']))
  {
    $foto = $_FILES['foto']['name'];
    $tmp  = $_FILES['foto']['tmp_name'];

    $fotobaru = date('dmYHis').$foto;
    $path = "foto/".$fotobaru;

    $nis = $kode .sprintf('%04s', $NoUrut).$rand;
    //.sprintf('%02s', $NoUrut) diformat menjadi 2 digit string
    if(move_uploaded_file($tmp, $path)){ // Cek apakah gambar berhasil diupload atau tidak
        // Proses simpan ke Database
        $sql = $db->exec("INSERT into siswa (nis, nama, jenkel, alamat, tempat_lahir, tanggal_lahir, notelp, foto, agama, kd_kelas)
        values('$nis', '$nama', '$jenkel', '$alamat', '$tmplahir', '$tgllahir', '$notelp', '$fotobaru', '$agama', '$kelas')");

        if($sql){ // Cek jika proses simpan ke database sukses atau tidak
            // Jika Sukses, Lakukan :
            header("location: ../index.php?hal=sw&tmb=success"); // Redirect ke halaman index.php
        }else{
            // Jika Gagal, Lakukan :
            header("location: ../index.php?hal=sw&tmb=fail");

        }
    }else{
        // Jika gambar gagal diupload, Lakukan :
        mysql_error();
    }
  }

  //PROSES UPDATE DATA
  else if (isset($_POST['update'])) 
  {

    $nise = $_POST['nis'];

    // Cek apakah user ingin mengubah fotonya atau tidak
    if(isset($_POST['ubah_foto'])){ // Jika user menceklis checkbox yang ada di form ubah, lakukan :

        $foto = $_FILES['foto']['name'];
        $tmp  = $_FILES['foto']['tmp_name'];

        $fotobaru = date('dmYHis').$foto;
        $path = "foto/".$fotobaru;

        // Proses upload
        if(move_uploaded_file($tmp, $path)){ // Cek apakah gambar berhasil diupload atau tidak
            // Query untuk menampilkan data siswa berdasarkan nis yang dikirim
            $stmt = $db->query ("SELECT * from siswa where nis='".$nise."'");
            $row = $stmt->fetch(PDO::FETCH_ASSOC);

            // Cek apakah file foto sebelumnya ada di folder foto
            if(file_exists("foto/".$row['foto'])){ // Jika foto ada
                unlink("foto/".$row['foto']); // Hapus file foto sebelumnya yang ada di folder foto
            }

            // Proses ubah data ke Database
            $sql = $db->exec("UPDATE siswa SET nama='$nama', alamat='$alamat', jenkel='$jenkel', tempat_lahir='$tmplahir', tanggal_lahir='$tgllahir', notelp='$notelp', agama='$agama', kd_kelas='$kelas', foto='$fotobaru' where nis='$nise'");

            if($sql){ // Cek jika proses simpan ke database sukses atau tidak
                // Jika Sukses, Lakukan :
                header("location: ../index.php?hal=sw&ubh=success"); // Redirect ke halaman
            }else{
                // Jika Gagal, Lakukan :
                //header("location: ../../index.php?hal=pgw&tmb=fail");
                die ("sql gagal");
            }
        }else{
            /*// Jika gambar gagal diupload, Lakukan :
            ?>
            <script >
                window.location=history.go(-1);
                document.write("Gagal upload gambar");
            </script>
            <?php*/
            die ('uploaded file false');
        }
    }else{ // Jika user tidak menceklis checkbox yang ada di form ubah, lakukan :
        // Proses ubah data ke Database
        $sql = $db->exec("UPDATE siswa SET nama='$nama', alamat='$alamat', jenkel='$jenkel', tempat_lahir='$tmplahir', tanggal_lahir='$tgllahir', notelp='$notelp', agama='$agama', kd_kelas='$kelas' where nis='$nise'");

        if($sql){ // Cek jika proses simpan ke database sukses atau tidak
            // Jika Sukses, Lakukan :
            header("location: ../index.php?hal=sw&ubh=success"); // Redirect ke halaman
        }
    }
  }

  //PROSES DELETE DATA
  else if(isset($_POST['hapus']))
  {
    $nish = $_POST['nis'];
      // Query untuk menampilkan data siswa berdasarkan nis yang dikirim
      $stmt = $db->query ("SELECT * from siswa where nis='".$nish."'");
      $row = $stmt->fetch(PDO::FETCH_ASSOC);

      // Cek apakah file foto sebelumnya ada di folder foto
      if(file_exists("foto/".$row['foto'])){ // Jika foto ada
           unlink("foto/".$row['foto']); // Hapus file foto sebelumnya yang ada di folder foto
      }

      // query SQL untuk delete data
      $sql = $db->exec("DELETE from siswa where nis='$nish'");

      if($sql){ // Cek jika proses simpan ke database sukses atau tidak
          // Jika Sukses, Lakukan :
          header("location: ../index.php?hal=sw&hps=success"); // Redirect ke halaman
      }else{
          // Jika Gagal, Lakukan :
          //header("location: ../../index.php?hal=pgw&tmb=fail");
          die ("sql gagal");
      }
  }
?>