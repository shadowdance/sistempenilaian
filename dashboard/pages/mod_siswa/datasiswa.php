            <div class="panel panel-default">
              <div class="panel-body"><h4><i class="fa fa-users fa-fw"></i>Data Siswa</h4></div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <?php
                        if (isset($_GET['tmb'])) {
                            if($_GET['tmb']=="success") {
                                ?>
                                <div class="alert alert-success" id="success-alert-input">
                                    Data berhasil disimpan.
                                </div>
                            <?php }else{ ?>
                                <div class="alert alert-danger" id="fail-alert-input">
                                    Data gagal disimpan.
                                </div>
                            <?php }
                        }
                        else if (isset($_GET['ubh'])) {
                                if($_GET['ubh']=="success") {
                                    ?>
                                    <div class="alert alert-success" id="success-alert-edit">
                                        Data berhasil diedit.
                                    </div>
                                <?php }else{ ?>
                                    <div class="alert alert-danger" id="fail-alert-edit">
                                        Data gagal diedit.
                                    </div>
                                <?php }
                        }
                        else if (isset($_GET['hps'])) {
                                if($_GET['hps']=="success") {
                                    ?>
                                    <div class="alert alert-success" id="success-alert-delete">
                                        Data berhasil dihapus.
                                    </div>
                                <?php }else{ ?>
                                    <div class="alert alert-danger" id="fail-alert-delete">
                                        Data gagal dihapus.
                                    </div>
                                <?php }
                        }
                    ?>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <button class="btn btn-default" data-toggle="modal" href="#" data-target="#entrysiswaModal"><i class="fa fa-plus"></i></button> Tambah Data Baru
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <?php
                                include('../connection/connection.php');
                                $stmt = $db->query("SELECT * FROM siswa a, kelas b, tahun_ajaran c where a.kd_kelas = b.kd_kelas and b.kd_ta = c.kd_ta");
                                echo'<table style="table-layout:fixed;" class="table table-striped table-bordered table-hover" id="dataTables-example">';
                                    echo '<thead>';
                                        echo '<tr>';
                                            echo '<th>NIS</th>';
                                            echo '<th>Nama</th>';
                                            echo '<th>Kelas</th>';
                                            echo '<th>Jenis Kelamin</th>';
                                            echo '<th>Alamat</th>';
                                            echo '<th>Aksi</th>';
                                        echo '</tr>';
                                    echo '</thead>';
                                    echo '<tbody>';
                                    while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                                            echo "<tr'>";       
                                                echo "<form method='POST' action='mod_siswa/proses.php'>";
                                                echo "<td style=' width:150px;  text-align:left; padding: 10px;vertical-align: middle;' class='text-uppercase'>";echo $row['nis'];echo"</td>";
                                                echo "<td style='width:110px;  text-align:left; vertical-align: middle;' class='text-capitalize'>";echo $row['nama'];"</td>";
                                                echo "<td style='width:110px;  text-align:left; vertical-align: middle;' class='text-uppercase'>";echo $row['nama_kelas'];echo " | ";echo $row['tahun_ajaran'];"</td>";
                                                echo "<td style='width:110px;  text-align:left; vertical-align: middle;' class='text-capitalize'>";echo $row['jenkel'];"</td>";
                                                echo "<td style='width:110px;  text-align:left; vertical-align: middle;' class='text-capitalize'>";echo $row['alamat'];"</td>";

                                                //Tombol aksi
                                                echo "<td style='width:110px;  text-align:left; vertical-align: middle;' class='text-capitalize'><button id='updatesiswa' class='btn btn-primary' data-toggle='modal' href='#' data-target='#updatesiswaModal".$row['nis']."' title='Edit'><i class='fa fa-edit'></i></button>&nbsp;<button title='Delete' class='btn btn-danger' href='#' data-toggle='modal' data-target='#deleteModal".$row['nis']."'><i class='fa fa-remove'></i></button>&nbsp;<button id='detilsiswa' class='btn btn-info' data-toggle='modal' href='#' data-target='#detilsiswaModal".$row['nis']."' title='Lihat Detil'><i class='fa fa-eye'></i></button>
                                                </td>";
                                                //End tombol aksi

                                                //MODAL DELETE
                                                echo '<div class="modal fade" id="deleteModal'.$row["nis"].'" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">';
                                                echo '<div class="modal-dialog modal-sm" role="document">';
                                                echo '<div class="modal-content">';
                                                echo '<div class="modal-header">
                                                    <button class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                    <h4 class="modal-title" id="myModalLabel">Confirm Delete</h4>
                                                </div>';
                                                echo "<div class='modal-body'>Kamu yakin ingin menghapus?
                                                </div>
                                                    <div class='modal-footer'>
                                                        <input type='hidden' value='$row[nis]' name='nis'>
                                                        <button type='button' class='btn btn-default' data-dismiss='modal'>Batal</button>
                                                        <button class='btn btn-danger' aria-label='Delete'type='submit' name='hapus'></span>Hapus</button>
                                                    </div>
                                                </div>";
                                                echo "</form>";
                                                //-END MODAL DELETE

                                            echo '</tr>';
                                    }
                                    echo '</tbody>';
                                echo '</table>';
                            ?>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->