<?php
    include('header.php');
?>

<?php
//cek jika url index.php tidak ada nilai
if(isset($_GET['hal'])){
    //berikan nilai index.php?hal
    switch($_GET['hal']){
        //saat index?hal=beranda, tampilkan isi konten dari beranda.php
        case 'beranda'  : include 'beranda.php'; break;

        //saat index?hal=gr, tampilkan isi konten dari mod_guru/dataguru.php
        case 'gr'      : include 'mod_guru/dataguru.php'; break;

        //saat index?hal=sw, tampilkan isi konten dari mod_siswa/datasiswa.php
        case 'sw'      : include 'mod_siswa/datasiswa.php'; break;

        //saat index?hal=kl, tampilkan isi konten dari mod_kelas/datakelas.php
        case 'kl'      : include 'mod_kelas/datakelas.php'; break;

        //saat index?hal=mp, tampilkan isi konten dari mod_mapel/datamapel.php
        case 'mp'      : include 'mod_mapel/datamapel.php'; break;

        //saat index?hal=ta, tampilkan isi konten dari mod_thajar/datathajar.php
        case 'ta'      : include 'mod_thajar/datathajar.php'; break;


        //selain itu tampilkan konten dari 404.php
        default : include '404.php';
    }
    
}else{
    //halaman default dari index berisi konten beranda.php
    include 'beranda.php';
}
?>

<!-- INPUT MODAL -->
<?php
    include('mod_guru/tambahguru.php');
    include('mod_kelas/tambahkelas.php');
    include('mod_mapel/tambahmapel.php');
    include('mod_thajar/tambahthajar.php');
    include('mod_siswa/tambahsiswa.php');
?>
<!-- END INPUT MODAL -->

<!-- UPDATE MODAL -->
<?php
    include('mod_guru/updateguru.php');
    include('mod_kelas/updatekelas.php');
    include('mod_mapel/updatemapel.php');
    include('mod_siswa/updatesiswa.php');
?>
<!-- END UPDATE MODAL -->

<!-- DETIL MODAL -->
<?php
    include('mod_guru/detilguru.php');
    include('mod_siswa/detilsiswa.php');
?>
<!-- END DETIL MODAL -->

<?php
    include('footer.php');
?>