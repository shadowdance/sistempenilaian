        <?php
          include('../connection/connection.php'); 
          //$id_bagian = $_POST['id_bagian'];
          
          $stmt = $db->query("select * from guru");

          //Fungsi Cek\
          class selected{
              function cek($val,$sel,$tipe){
                  if($val==$sel){
                      switch($tipe){
                          case 'select' :echo "selected"; break;
                          case 'radio' :echo "checked"; break;
                      }
                  }else{
                      echo "";
                  }
              }
          }
          $ob = new selected();
        
          //<!-- update bagian modal -->
          while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) { 
        ?>
        <div <?php echo 'id="updateguruModal'.$row['nip'].'"' ?> class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3 id="myModalLabel">Update Guru</h3>
              </div>
              <div class="modal-body">
                <form method="POST" action="mod_guru/proses.php" enctype="multipart/form-data">
                  <div class="form-group"><label>NIP</label><input class="form-control required text-uppercase" placeholder="Input ID Guru" data-placement="top" data-trigger="manual" type="text" name="nip" maxlength="10" value="<?php echo $row['nip']; ?>" readonly></div>
                  <div class="form-group">
                    <label>Foto (maks 2mb)</label>
                      <div class="checkbox">
                        <label>
                          <input type="checkbox" name="ubah_foto" value="true"> Ceklis jika ingin mengubah foto<br>
                          <input type="file" name="foto">
                        </label>
                      </div>
                  </div>
                  <div class="form-group"><label>Nama</label><input required class="form-control required text-capitalize" placeholder="Input Nama" data-placement="top" data-trigger="manual" type="text" name="nama" value="<?php echo $row['nama']; ?>"></div>
                  <div class="form-group"><label>Jenis Kelamin</label>
                    <div class="selectContainer">
                        <select class="form-control" name="jenkel">
                            <option <?php $ob->cek("Laki-laki",$row['jenkel'],"select") ?> value="Laki-laki">Laki-laki</option>
                            <option <?php $ob->cek("Perempuan",$row['jenkel'],"select") ?> value="Perempuan">Perempuan</option>
                        </select>
                    </div>
                  </div>
                  <div class="form-group"><label>Tempat Lahir</label><input required class="form-control required text-capitalize" placeholder="Input Tempat Lahir" data-placement="top" data-trigger="manual" type="text" name="tmplahir" value="<?php echo $row['tempat_lahir'] ?>"></div>
                  <div class="form-group"><label>Tanggal Lahir</label><input required class="form-control required text-capitalize" placeholder="yyyy-mm-dd" data-placement="top" data-trigger="manual" type="date" name="tgllahir" value="<?php echo $row['tanggal_lahir'] ?>"></div>
                  <div class="form-group"><label>Alamat</label>
                    <textarea class="form-control" name="alamat"><?php echo $row['alamat'] ?></textarea>
                  </div>
                  <div class="form-group"><label>No. Telp</label><input required class="form-control required text-capitalize" placeholder="Input No Telp" data-placement="top" data-trigger="manual" type="text" name="notelp" id="notelpguruedit" maxlength="13" value="<?php echo $row['notelp']; ?>"></div>
                  <div class="form-group"><button type="submit" class="btn btn-success pull-center" name="update">Update</button> <p class="help-block pull-left text-danger hide" id="form-error">&nbsp; The form is not valid. </p></div>
                </form>
              </div>
            </div>
          </div>
        </div>
        <?php 
          } 
        ?>
        <!-- /.update bagian modal -->