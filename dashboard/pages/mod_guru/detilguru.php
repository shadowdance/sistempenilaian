        <?php
          include('../connection/connection.php'); 
          //$id_bagian = $_POST['id_bagian'];
          
          $stmt = $db->query("select * from guru");
        
          //<!-- update bagian modal -->
          while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) { 
        ?>
        <!-- detil guru modal -->
        <div <?php echo 'id="detilguruModal'.$row['nip'].'"' ?> class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3 id="myModalLabel">Detil Guru</h3>
              </div>
              <div class="modal-body">
                <form method="POST" action="mod_guru/proses.php" enctype="multipart/form-data">
                  <table class="table table-responsive" border="0" style="margin-top: -40px;">
                    <thead>
                      <td width="25%" ></td>
                      <td width="5%" ></td>
                      <td width="50%" ></td>
                      <td width="20%" ></td>
                    </thead>
                    <tbody>
                      <tr>
                        <td>NIP</td>
                        <td>:</td>
                        <td><?php echo $row['nip'];?></td>
                        <td rowspan="12"><img  src="mod_guru/foto/<?php echo $row['foto'];?>" alt="foto-guru" width="150" height="150" onmousedown="return false" oncontexmenu="return false" onselectstart="return false"/></td>
                      </tr>
                      <tr>
                        <td>Nama Lengkap</td>
                        <td>:</td>
                        <td style="text-transform:capitalize;"><?php echo $row['nama'];?></td>
                      </tr>
                      <tr>
                        <td>Jenis Kelamin</td>
                        <td>:</td>
                        <td style="text-transform:capitalize;"><?php echo $row['jenkel'];?></td>
                      </tr>
                      <tr>
                        <td>Tempat Lahir</td>
                        <td>:</td>
                        <td style="text-transform:capitalize;"><?php echo $row['tempat_lahir'];?></td>
                      </tr>
                      <tr>
                        <td>Tanggal Lahir</td>
                        <td>:</td>
                        <td style="text-transform:capitalize;"><?php echo $row['tanggal_lahir'];?></td>
                      </tr>
                      <tr>
                        <td>Alamat</td>
                        <td>:</td>
                        <td style="text-transform:capitalize;"><?php echo $row['alamat'];?></td>
                      </tr>
                      <tr>
                        <td>Nomor Telepon</td>
                        <td>:</td>
                        <td style="text-transform:capitalize;"><?php echo $row['notelp'];?></td>
                      </tr>
                    </tbody>
                  </table>
                </form>
              </div>
            </div>
          </div>
        </div>
        <?php
          }
        ?>
        <!-- /.detil guru modal -->