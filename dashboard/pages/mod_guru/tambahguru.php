        <!-- entry guru modal -->
        <div id="entryguruModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3 id="myModalLabel">Tambah Guru</h3>
              </div>
              <div class="modal-body">
                <form method="POST" action="mod_guru/proses.php" enctype="multipart/form-data">
                  <div class="form-group"><label>Foto (maks 2mb)</label><input required class="form-control" data-placement="top" data-trigger="manual" type="file" name="foto"></div>
                  <!-- INPUTAN -->
                  <div class="form-group"><label>Nama</label><input required class="form-control required text-capitalize" placeholder="Input Nama guru" data-placement="top" data-trigger="manual" type="text" name="nama"></div>
                  <!-- INPUTAN -->
                  <div class="form-group"><label>Jenis Kelamin</label>
                    <div class="selectContainer">
                        <select class="form-control" name="jenkel">
                            <option value="Laki-laki">Laki-laki</option>
                            <option value="Perempuan">Perempuan</option>
                        </select>
                    </div>
                  </div>
                  <div class="form-group"><label>Tempat Lahir</label><input required class="form-control required text-capitalize" placeholder="Input Tempat Lahir" data-placement="top" data-trigger="manual" type="text" name="tmplahir"></div>
                  <div class="form-group"><label>Tanggal Lahir</label><input required class="form-control" placeholder="yyyy-mm-dd" data-placement="top" data-trigger="manual" type="date" name="tgllahir"></div>
                  <div class="form-group"><label>Alamat</label>
                    <textarea class="form-control" name="alamat" required></textarea>
                  </div>
                  <div class="form-group"><label>Nomor Telepon</label><input required class="form-control required" placeholder="xxxxxxxxxxxxx" data-placement="top" data-trigger="manual" type="text" name="notelp" id="notelpguru" maxlength="13"></div>
                  <div class="form-group"><button type="submit" class="btn btn-success pull-center" name="tambah">Submit</button> <p class="help-block pull-left text-danger hide" id="form-error">&nbsp; The form is not valid. </p></div>
                </form>
              </div>
            </div>
          </div>
        </div>
        <!-- /.entry guru modal -->