<?php
	include('../../connection/connection.php');
    $nama = $_POST['nama'];
    $alamat = $_POST['alamat'];
    $notelp = $_POST['notelp'];
    $jenkel = $_POST['jenkel'];
    $tmplahir = $_POST['tmplahir'];
    $tgllahir = $_POST['tgllahir'];
    $pass = $tgllahir;

    //cari id terbesar
    $stmt = $db->query('SELECT max(nip) as maxNO FROM guru');
    $row = $stmt->fetch(PDO::FETCH_ASSOC);
    $idMax = $row['maxNO']; //0
    //setelah membaca id terbesar, lanjut mencari nomor urut id dari id terbesar
    $NoUrut = (int) substr($idMax, 2, 4);
    $NoUrut++; //nomor urut +1
    $kode = "G";

  //PROSES TAMBAH DATA
  if(isset($_POST['tambah']))
  {
    $foto = $_FILES['foto']['name'];
    $tmp  = $_FILES['foto']['tmp_name'];

    $fotobaru = date('dmYHis').$foto;
    $path = "foto/".$fotobaru;

    $nip = $kode .sprintf('%04s', $NoUrut);
    //.sprintf('%02s', $NoUrut) diformat menjadi 4 digit string
    if(move_uploaded_file($tmp, $path)){ // Cek apakah gambar berhasil diupload atau tidak
        // Proses simpan ke Database
        $sql = $db->exec("INSERT into guru (nip, nama, jenkel, alamat, tempat_lahir, tanggal_lahir, notelp, foto, password)
        values('$nip', '$nama', '$jenkel', '$alamat', '$tmplahir', '$tgllahir', '$notelp', '$fotobaru', '$pass')");

        if($sql){ // Cek jika proses simpan ke database sukses atau tidak
            // Jika Sukses, Lakukan :
            header("location: ../index.php?hal=gr&tmb=success"); // Redirect ke halaman index.php
        }else{
            // Jika Gagal, Lakukan :
            header("location: ../index.php?hal=gr&tmb=fail");

        }
    }else{
        // Jika gambar gagal diupload, Lakukan :
        mysql_error();
    }
  }

  //PROSES UPDATE DATA
  else if (isset($_POST['update'])) 
  {

    $nipe = $_POST['nip'];

    // Cek apakah user ingin mengubah fotonya atau tidak
    if(isset($_POST['ubah_foto'])){ // Jika user menceklis checkbox yang ada di form ubah, lakukan :

        $foto = $_FILES['foto']['name'];
        $tmp  = $_FILES['foto']['tmp_name'];

        $fotobaru = date('dmYHis').$foto;
        $path = "foto/".$fotobaru;

        // Proses upload
        if(move_uploaded_file($tmp, $path)){ // Cek apakah gambar berhasil diupload atau tidak
            // Query untuk menampilkan data guru berdasarkan NIP yang dikirim
            $stmt = $db->query ("SELECT * from guru where nip='".$nipe."'");
            $row = $stmt->fetch(PDO::FETCH_ASSOC);

            // Cek apakah file foto sebelumnya ada di folder foto
            if(file_exists("foto/".$row['foto'])){ // Jika foto ada
                unlink("foto/".$row['foto']); // Hapus file foto sebelumnya yang ada di folder foto
            }

            // Proses ubah data ke Database
            $sql = $db->exec("UPDATE guru SET nama='$nama', alamat='$alamat', jenkel='$jenkel', tempat_lahir='$tmplahir', tanggal_lahir='$tgllahir', notelp='$notelp', foto='$fotobaru' where nip='$nipe'");

            if($sql){ // Cek jika proses simpan ke database sukses atau tidak
                // Jika Sukses, Lakukan :
                header("location: ../index.php?hal=gr&ubh=success"); // Redirect ke halaman
            }else{
                // Jika Gagal, Lakukan :
                die ("sql gagal");
            }
        }else{
            /*// Jika gambar gagal diupload, Lakukan :
            ?>
            <script >
                window.location=history.go(-1);
                document.write("Gagal upload gambar");
            </script>
            <?php*/
            die ('uploaded file false');
        }
    }else{ // Jika user tidak menceklis checkbox yang ada di form ubah, lakukan :
        // Proses ubah data ke Database
        $sql = $db->exec("UPDATE guru SET nama='$nama', alamat='$alamat', jenkel='$jenkel', tempat_lahir='$tmplahir', tanggal_lahir='$tgllahir', notelp='$notelp' where nip='$nipe'");

        if($sql){ // Cek jika proses simpan ke database sukses atau tidak
            // Jika Sukses, Lakukan :
            header("location: ../index.php?hal=gr&ubh=success"); // Redirect ke halaman
        }
    }
  }

  //PROSES DELETE DATA
  else if(isset($_POST['hapus']))
  {
    $niph = $_POST['nip'];
      // Query untuk menampilkan data guru berdasarkan NIP yang dikirim
      $stmt = $db->query ("SELECT * from guru where nip='".$niph."'");
      $row = $stmt->fetch(PDO::FETCH_ASSOC);

      // Cek apakah file foto sebelumnya ada di folder foto
      if(file_exists("foto/".$row['foto'])){ // Jika foto ada
           unlink("foto/".$row['foto']); // Hapus file foto sebelumnya yang ada di folder foto
      }

      // query SQL untuk delete data
      $sql = $db->exec("DELETE from guru where nip='$niph'");

      if($sql){ // Cek jika proses simpan ke database sukses atau tidak
          // Jika Sukses, Lakukan :
          header("location: ../index.php?hal=gr&hps=success"); // Redirect ke halaman
      }else{
          // Jika Gagal, Lakukan :
          die ("sql gagal");
      }
  }
?>