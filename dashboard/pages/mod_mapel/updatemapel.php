        <?php
          include('../connection/connection.php'); 
          //$id_bagian = $_POST['id_bagian'];
          
          $stmt = $db->query("select * from mata_pelajaran");
        
          //<!-- update bagian modal -->
          while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) { 
        ?>
        <div <?php echo 'id="updatemapelModal'.$row['kd_mapel'].'"' ?> class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3 id="myModalLabel">Update Mata Pelajaran</h3>
              </div>
              <div class="modal-body">
                <form method="POST" action="mod_mapel/proses.php" enctype="multipart/form-data">
                  <div class="form-group"><label>Kode Mata Pelajaran</label><input class="form-control required text-uppercase" placeholder="Input ID mapel" data-placement="top" data-trigger="manual" type="text" name="kd_mapel" value="<?php echo $row['kd_mapel']; ?>" readonly></div>
                  <div class="form-group"><label>Nama Mata Pelajaran</label><input required class="form-control required text-uppercase" placeholder="Input Nama" data-placement="top" data-trigger="manual" type="text" name="namamapel" value="<?php echo $row['nama_mapel']; ?>"></div>
                  <div class="form-group"><button type="submit" class="btn btn-success pull-center" name="update">Update</button> <p class="help-block pull-left text-danger hide" id="form-error">&nbsp; The form is not valid. </p></div>
                </form>
              </div>
            </div>
          </div>
        </div>
        <?php 
          } 
        ?>
        <!-- /.update bagian modal -->