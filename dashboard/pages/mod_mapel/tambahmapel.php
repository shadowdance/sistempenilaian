        <!-- entry mapel modal -->
        <div id="entrymapelModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3 id="myModalLabel">Tambah Mata Pelajaran</h3>
              </div>
              <div class="modal-body">
                <form method="POST" action="mod_mapel/proses.php" enctype="multipart/form-data">
                  <div class="form-group"><label>Nama Mata Pelajaran</label><input required class="form-control text-uppercase" placeholder="Input Nama Mata Pelajaran" data-placement="top" data-trigger="manual" type="text" name="namamapel"></div>
                  <div class="form-group"><button type="submit" class="btn btn-success pull-center" name="tambah">Submit</button> <p class="help-block pull-left text-danger hide" id="form-error">&nbsp; The form is not valid. </p></div>
                </form>
              </div>
            </div>
          </div>
        </div>
        <!-- /.entry mapel modal -->