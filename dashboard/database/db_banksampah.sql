-- phpMyAdmin SQL Dump
-- version 4.4.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 11, 2017 at 01:16 PM
-- Server version: 5.6.25
-- PHP Version: 5.6.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_banksampah`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_customer`
--

CREATE TABLE IF NOT EXISTS `tb_customer` (
  `id_customer` int(10) NOT NULL,
  `nm_customer` varchar(30) NOT NULL,
  `alamat` text NOT NULL,
  `RT` char(2) NOT NULL,
  `RW` char(2) NOT NULL,
  `no_telp` varchar(15) NOT NULL,
  `email` varchar(15) NOT NULL,
  `status` enum('1','0') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_customer`
--

INSERT INTO `tb_customer` (`id_customer`, `nm_customer`, `alamat`, `RT`, `RW`, `no_telp`, `email`, `status`) VALUES
(2017071901, 'tomas', 'petukangan 2', '22', '09', '0898312312', 'tomas@ymail.com', '1'),
(2017072002, 'ozo', 'ciledug', '11', '12', '0823131312313', 'ozo@gmail.com', '1');

-- --------------------------------------------------------

--
-- Table structure for table `tb_detilpenjualan`
--

CREATE TABLE IF NOT EXISTS `tb_detilpenjualan` (
  `no_transaksi` char(16) NOT NULL,
  `id_sampah` tinyint(2) NOT NULL,
  `jumlah` int(2) NOT NULL DEFAULT '0',
  `hargajual` decimal(10,2) NOT NULL DEFAULT '0.00',
  `total` decimal(10,2) NOT NULL DEFAULT '0.00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_detilpenjualan`
--

INSERT INTO `tb_detilpenjualan` (`no_transaksi`, `id_sampah`, `jumlah`, `hargajual`, `total`) VALUES
('JS110820170001', 2, 3, '5000.00', '15000.00'),
('JS110820170001', 3, 4, '6500.00', '26000.00'),
('JS110820170001', 4, 7, '4600.00', '32200.00'),
('JS110820170001', 1, 2, '6300.00', '12600.00');

--
-- Triggers `tb_detilpenjualan`
--
DELIMITER $$
CREATE TRIGGER `stok_after_cjual` AFTER DELETE ON `tb_detilpenjualan`
 FOR EACH ROW update tb_sampah SET stok = stok + OLD.jumlah
        WHERE id_sampah = OLD.id_sampah;
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `stok_after_jual` AFTER INSERT ON `tb_detilpenjualan`
 FOR EACH ROW update tb_sampah SET stok = stok - NEW.jumlah
        WHERE id_sampah = NEW.id_sampah
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `tb_detiltabungan`
--

CREATE TABLE IF NOT EXISTS `tb_detiltabungan` (
  `no_transaksi` char(16) NOT NULL,
  `id_nasabah` int(10) NOT NULL,
  `id_sampah` tinyint(2) NOT NULL,
  `jumlah` int(2) NOT NULL,
  `total` decimal(10,2) NOT NULL DEFAULT '0.00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_detiltabungan`
--

INSERT INTO `tb_detiltabungan` (`no_transaksi`, `id_nasabah`, `id_sampah`, `jumlah`, `total`) VALUES
('TB080820170001', 2017240701, 2, 6, '39000.00'),
('TB100820170002', 2017260702, 1, 10, '50000.00'),
('TB100820170002', 2017260702, 2, 10, '65000.00'),
('TB110820170003', 2017240701, 2, 2, '13000.00'),
('TB110820170003', 2017240701, 3, 5, '20000.00');

--
-- Triggers `tb_detiltabungan`
--
DELIMITER $$
CREATE TRIGGER `stok_after_cnabung` AFTER DELETE ON `tb_detiltabungan`
 FOR EACH ROW BEGIN
update tb_sampah SET stok = stok - OLD.jumlah
        WHERE id_sampah = OLD.id_sampah;
				
update tb_nasabah SET tabungan = tabungan - OLD.total
				WHERE id_nasabah = OLD.id_nasabah;
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `stok_after_nabung` AFTER INSERT ON `tb_detiltabungan`
 FOR EACH ROW BEGIN
update tb_sampah SET stok = stok + NEW.jumlah
        WHERE id_sampah = NEW.id_sampah;
				
update tb_nasabah SET tabungan = tabungan + NEW.total
				WHERE id_nasabah = NEW.id_nasabah;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `tb_nasabah`
--

CREATE TABLE IF NOT EXISTS `tb_nasabah` (
  `id_nasabah` int(10) NOT NULL,
  `no_ktp` char(16) NOT NULL,
  `nm_nasabah` varchar(30) DEFAULT NULL,
  `jenkel` enum('1','0') NOT NULL,
  `alamat` text,
  `RT` char(2) NOT NULL,
  `RW` char(2) NOT NULL,
  `no_telp` varchar(15) DEFAULT NULL,
  `email` varchar(15) DEFAULT NULL,
  `tabungan` decimal(10,2) NOT NULL DEFAULT '0.00',
  `status` enum('1','0') DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_nasabah`
--

INSERT INTO `tb_nasabah` (`id_nasabah`, `no_ktp`, `nm_nasabah`, `jenkel`, `alamat`, `RT`, `RW`, `no_telp`, `email`, `tabungan`, `status`) VALUES
(2017240701, '1632313123232313', 'hendry', '1', 'bahari 2', '08', '07', '089673751885', 'hendryrpl@gmail', '72000.00', '1'),
(2017260702, '1523123123123111', 'purwo', '1', 'pd ranji', '07', '01', '0832313231321', 'purwo@gmail.com', '115000.00', '1');

-- --------------------------------------------------------

--
-- Table structure for table `tb_penjualan`
--

CREATE TABLE IF NOT EXISTS `tb_penjualan` (
  `tgl_transaksi` date NOT NULL DEFAULT '0000-00-00',
  `no_transaksi` char(16) NOT NULL,
  `id_petugas` char(10) NOT NULL,
  `id_customer` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_penjualan`
--

INSERT INTO `tb_penjualan` (`tgl_transaksi`, `no_transaksi`, `id_petugas`, `id_customer`) VALUES
('2017-08-11', 'JS110820170001', 'admin', 2017071901);

-- --------------------------------------------------------

--
-- Table structure for table `tb_petugas`
--

CREATE TABLE IF NOT EXISTS `tb_petugas` (
  `id_petugas` char(10) NOT NULL,
  `password` varchar(20) DEFAULT NULL,
  `nm_petugas` varchar(30) DEFAULT NULL,
  `alamat` text,
  `no_telp` varchar(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_petugas`
--

INSERT INTO `tb_petugas` (`id_petugas`, `password`, `nm_petugas`, `alamat`, `no_telp`) VALUES
('admin', 'admin', 'hendry', 'dsadadsa', '32312321');

-- --------------------------------------------------------

--
-- Table structure for table `tb_sampah`
--

CREATE TABLE IF NOT EXISTS `tb_sampah` (
  `id_sampah` tinyint(2) NOT NULL,
  `jenis_sampah` varchar(20) DEFAULT NULL,
  `stok` int(5) NOT NULL DEFAULT '0',
  `hargabeli` decimal(10,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_sampah`
--

INSERT INTO `tb_sampah` (`id_sampah`, `jenis_sampah`, `stok`, `hargabeli`) VALUES
(1, 'Alumunium', 6, '5000.00'),
(2, 'Besi', 9, '6500.00'),
(3, 'Kaleng', 11, '4000.00'),
(4, 'Koran', 3, '3800.00'),
(5, 'Kardus', 0, '3700.00'),
(6, 'Emberan/Plastik', 0, '4000.00'),
(7, 'Gelas Mineral', 0, '4000.00'),
(8, 'Botol Mineral', 0, '4200.00'),
(9, 'Botol Kaca Pet A', 0, '4300.00'),
(10, 'Buku', 0, '7000.00'),
(11, 'Tutup Botol', 0, '3000.00'),
(12, 'SP/Boncos', 0, '3500.00');

-- --------------------------------------------------------

--
-- Table structure for table `tb_tabungan`
--

CREATE TABLE IF NOT EXISTS `tb_tabungan` (
  `tgl_transaksi` date NOT NULL DEFAULT '0000-00-00',
  `no_transaksi` char(16) NOT NULL,
  `id_nasabah` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_tabungan`
--

INSERT INTO `tb_tabungan` (`tgl_transaksi`, `no_transaksi`, `id_nasabah`) VALUES
('2017-08-08', 'TB080820170001', 2017240701),
('2017-08-10', 'TB100820170002', 2017260702),
('2017-08-11', 'TB110820170003', 2017240701);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_customer`
--
ALTER TABLE `tb_customer`
  ADD PRIMARY KEY (`id_customer`);

--
-- Indexes for table `tb_detilpenjualan`
--
ALTER TABLE `tb_detilpenjualan`
  ADD KEY `no_transaksi` (`no_transaksi`),
  ADD KEY `id_sampah` (`id_sampah`);

--
-- Indexes for table `tb_detiltabungan`
--
ALTER TABLE `tb_detiltabungan`
  ADD KEY `no_transaksi` (`no_transaksi`),
  ADD KEY `id_sampah` (`id_sampah`),
  ADD KEY `id_nasabah` (`id_nasabah`);

--
-- Indexes for table `tb_nasabah`
--
ALTER TABLE `tb_nasabah`
  ADD PRIMARY KEY (`id_nasabah`);

--
-- Indexes for table `tb_penjualan`
--
ALTER TABLE `tb_penjualan`
  ADD PRIMARY KEY (`no_transaksi`),
  ADD KEY `id_petugas` (`id_petugas`),
  ADD KEY `id_customer` (`id_customer`);

--
-- Indexes for table `tb_petugas`
--
ALTER TABLE `tb_petugas`
  ADD PRIMARY KEY (`id_petugas`);

--
-- Indexes for table `tb_sampah`
--
ALTER TABLE `tb_sampah`
  ADD PRIMARY KEY (`id_sampah`);

--
-- Indexes for table `tb_tabungan`
--
ALTER TABLE `tb_tabungan`
  ADD PRIMARY KEY (`no_transaksi`),
  ADD KEY `no_nasabah` (`id_nasabah`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tb_detilpenjualan`
--
ALTER TABLE `tb_detilpenjualan`
  ADD CONSTRAINT `tb_detilpenjualan_ibfk_4` FOREIGN KEY (`id_sampah`) REFERENCES `tb_sampah` (`id_sampah`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `tb_detiltabungan`
--
ALTER TABLE `tb_detiltabungan`
  ADD CONSTRAINT `tb_detiltabungan_ibfk_3` FOREIGN KEY (`id_sampah`) REFERENCES `tb_sampah` (`id_sampah`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `tb_detiltabungan_ibfk_4` FOREIGN KEY (`id_nasabah`) REFERENCES `tb_nasabah` (`id_nasabah`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `tb_penjualan`
--
ALTER TABLE `tb_penjualan`
  ADD CONSTRAINT `tb_penjualan_ibfk_3` FOREIGN KEY (`id_customer`) REFERENCES `tb_customer` (`id_customer`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tb_tabungan`
--
ALTER TABLE `tb_tabungan`
  ADD CONSTRAINT `tb_tabungan_ibfk_2` FOREIGN KEY (`id_nasabah`) REFERENCES `tb_nasabah` (`id_nasabah`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
