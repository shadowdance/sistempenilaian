-- phpMyAdmin SQL Dump
-- version 4.4.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Sep 10, 2017 at 10:51 AM
-- Server version: 5.6.25
-- PHP Version: 5.6.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_sdpi`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `username` varchar(20) NOT NULL,
  `password` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`username`, `password`) VALUES
('admin', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `guru`
--

CREATE TABLE IF NOT EXISTS `guru` (
  `nip` char(5) NOT NULL,
  `nama` varchar(35) NOT NULL,
  `tempat_lahir` varchar(30) NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `jenkel` enum('Laki-laki','Perempuan') NOT NULL,
  `alamat` text NOT NULL,
  `notelp` char(15) NOT NULL,
  `password` char(20) NOT NULL,
  `foto` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `guru`
--

INSERT INTO `guru` (`nip`, `nama`, `tempat_lahir`, `tanggal_lahir`, `jenkel`, `alamat`, `notelp`, `password`, `foto`) VALUES
('G0001', 'Nanda Kusmianda', 'Jakarta Selatan', '1995-01-10', 'Laki-laki', 'Jl.Damai Rt 005/005 Kel. Petukangan Selatan Kec.Pesanggrahan', '6281283970655', '1995-01-10', '310820170705001.png'),
('G0002', 'purwo', 'Tangerang', '1997-10-21', 'Laki-laki', 'pejaten', '0823131231211', '1997-10-21', '310820170705422.png'),
('G0003', 'hendry', 'blora', '1997-11-20', 'Laki-laki', 'jl. bahari 2', '089873751885', '1997-11-20', '31082017070626h.jpg'),
('G0004', 'ozi', 'jakarta', '1997-06-23', 'Laki-laki', 'cilandak', '0823398319283', '1997-06-23', '31082017070829hn.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `kelas`
--

CREATE TABLE IF NOT EXISTS `kelas` (
  `kd_kelas` char(2) NOT NULL,
  `nama_kelas` char(2) NOT NULL,
  `nip` char(5) NOT NULL,
  `kd_ta` char(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kelas`
--

INSERT INTO `kelas` (`kd_kelas`, `nama_kelas`, `nip`, `kd_ta`) VALUES
('K1', '1a', 'G0001', 'TA2017'),
('K2', '1b', 'G0002', 'TA2017');

-- --------------------------------------------------------

--
-- Table structure for table `mata_pelajaran`
--

CREATE TABLE IF NOT EXISTS `mata_pelajaran` (
  `kd_mapel` char(5) NOT NULL,
  `nama_mapel` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mata_pelajaran`
--

INSERT INTO `mata_pelajaran` (`kd_mapel`, `nama_mapel`) VALUES
('M0001', 'tematik'),
('M0002', 'tik'),
('M0003', 'bahasa arab');

-- --------------------------------------------------------

--
-- Table structure for table `nilai`
--

CREATE TABLE IF NOT EXISTS `nilai` (
  `kd_nilai` int(11) NOT NULL,
  `nis` char(10) NOT NULL,
  `kd_mapel` char(5) NOT NULL,
  `semester` varchar(10) NOT NULL,
  `nil_tugas` double(5,2) DEFAULT NULL,
  `nil_uts` double(5,2) DEFAULT NULL,
  `nil_uas` double(5,2) NOT NULL DEFAULT '0.00',
  `nil_akhir` double(5,2) NOT NULL DEFAULT '0.00',
  `keterangan` text NOT NULL,
  `nip` char(5) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=114 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `nilai`
--

INSERT INTO `nilai` (`kd_nilai`, `nis`, `kd_mapel`, `semester`, `nil_tugas`, `nil_uts`, `nil_uas`, `nil_akhir`, `keterangan`, `nip`) VALUES
(110, '0831000168', 'M0001', 'Ganjil', 87.00, 99.00, 67.00, 84.33, 'das', 'G0001'),
(111, '0831000260', 'M0001', 'Ganjil', 87.00, 95.00, 89.00, 90.33, 'da', 'G0001'),
(112, '0901000398', 'M0001', 'Ganjil', 90.00, 66.00, 97.00, 84.33, 'dsa', 'G0002');

-- --------------------------------------------------------

--
-- Table structure for table `siswa`
--

CREATE TABLE IF NOT EXISTS `siswa` (
  `nis` char(10) NOT NULL,
  `nama` varchar(35) NOT NULL,
  `tempat_lahir` varchar(25) NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `jenkel` enum('Laki-laki','Perempuan') NOT NULL,
  `agama` varchar(15) NOT NULL,
  `alamat` text NOT NULL,
  `notelp` char(15) NOT NULL,
  `kd_kelas` char(2) NOT NULL,
  `foto` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `siswa`
--

INSERT INTO `siswa` (`nis`, `nama`, `tempat_lahir`, `tanggal_lahir`, `jenkel`, `agama`, `alamat`, `notelp`, `kd_kelas`, `foto`) VALUES
('0831000168', 'agung', 'Jakarta Selatan', '1997-06-10', 'Laki-laki', 'Islam', 'petukangan', '0823316643434', 'K1', '31082017080210duel.jpg'),
('0831000260', 'doko', 'ambon', '1997-09-15', 'Laki-laki', 'Islam', 'pd maharta', '0856123542313', 'K1', '31082017080721kzl.jpg'),
('0901000398', 'pandu', 'bogor', '1996-10-22', 'Laki-laki', 'Islam', 'sdadaa', '0892323132312', 'K2', '01092017111347hhhhhhhh.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tahun_ajaran`
--

CREATE TABLE IF NOT EXISTS `tahun_ajaran` (
  `kd_ta` char(6) NOT NULL,
  `val_ta` int(4) NOT NULL,
  `tahun_ajaran` char(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tahun_ajaran`
--

INSERT INTO `tahun_ajaran` (`kd_ta`, `val_ta`, `tahun_ajaran`) VALUES
('TA2017', 2017, '2017/2018'),
('TA2018', 2018, '2018/2019'),
('TA2019', 2019, '2019/2020');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`username`);

--
-- Indexes for table `guru`
--
ALTER TABLE `guru`
  ADD PRIMARY KEY (`nip`);

--
-- Indexes for table `kelas`
--
ALTER TABLE `kelas`
  ADD PRIMARY KEY (`kd_kelas`),
  ADD KEY `nip` (`nip`),
  ADD KEY `kd_ta` (`kd_ta`);

--
-- Indexes for table `mata_pelajaran`
--
ALTER TABLE `mata_pelajaran`
  ADD PRIMARY KEY (`kd_mapel`);

--
-- Indexes for table `nilai`
--
ALTER TABLE `nilai`
  ADD PRIMARY KEY (`kd_nilai`),
  ADD KEY `nis` (`nis`),
  ADD KEY `kd_mapel` (`kd_mapel`),
  ADD KEY `nip` (`nip`);

--
-- Indexes for table `siswa`
--
ALTER TABLE `siswa`
  ADD PRIMARY KEY (`nis`),
  ADD KEY `kd_kelas` (`kd_kelas`);

--
-- Indexes for table `tahun_ajaran`
--
ALTER TABLE `tahun_ajaran`
  ADD PRIMARY KEY (`kd_ta`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `nilai`
--
ALTER TABLE `nilai`
  MODIFY `kd_nilai` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=114;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `kelas`
--
ALTER TABLE `kelas`
  ADD CONSTRAINT `kelas_ibfk_1` FOREIGN KEY (`nip`) REFERENCES `guru` (`nip`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `kelas_ibfk_2` FOREIGN KEY (`kd_ta`) REFERENCES `tahun_ajaran` (`kd_ta`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `nilai`
--
ALTER TABLE `nilai`
  ADD CONSTRAINT `nilai_ibfk_1` FOREIGN KEY (`kd_mapel`) REFERENCES `mata_pelajaran` (`kd_mapel`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `nilai_ibfk_3` FOREIGN KEY (`nis`) REFERENCES `siswa` (`nis`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `nilai_ibfk_4` FOREIGN KEY (`nip`) REFERENCES `guru` (`nip`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `siswa`
--
ALTER TABLE `siswa`
  ADD CONSTRAINT `siswa_ibfk_1` FOREIGN KEY (`kd_kelas`) REFERENCES `kelas` (`kd_kelas`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
